package org.zeissler;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.Scanner;

public class HttpsCrawler {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Connection.Response response;
        System.out.println("This tools checks for URl redirects (to find http->https redirects). Keep in mind that some sites redirect domain.com to www.domain.com\nPlease type the domains to test, one per line, preferably including the www part.");
        while(scanner.hasNext()){
            try {
                String url = scanner.nextLine();
                response = Jsoup.connect("http://" + url)
                        .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                        .timeout(10000)
                        .followRedirects(false)
                        .execute();

                int statusCode = response.statusCode();
                if(statusCode == 301 || statusCode == 302 ||statusCode == 307) {
                    System.out.println(" -> redirect found! received status code : " + statusCode + ". target url:  " + response.header("location"));
                }
                else {
                    //TBD
                    System.out.println(" -> !! NO REDIRECT !! status code : " + statusCode);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
